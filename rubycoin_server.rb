require 'sinatra'
require "sinatra/reloader" if development?
require 'colorize'

get '/' do
  'Welcome'
end

BALANCES ||= {
  monica: 1_000
}

PASSWORDS = {
  monica: 'Test_1234'
}

# @param user - the user you want to know the balance of
get '/balance/:user' do |user|
  if BALANCES.has_key? user.to_sym
    balance = BALANCES[user.to_sym]

    "User: #{user} | Balance: #{balance}M$"
  else
    "No such user"
  end
end

# @params name - the name of the user being created
# @params password - the password for the user being created
# sets balance to 0 unless the user is already present
post '/users' do
  name = params['name'].to_sym
  password = params['password']

  if BALANCES.has_key? name
    "User already present"
  else
    BALANCES[name] = 0
    PASSWORDS[name] = password

    "User created. User: #{name} | Balance: #{balance}M$"
  end

  puts BALANCES
end

# @param from - account sending the money
# @param to - account receiving the money
# @param amount - amount to be transferred in cents
# @param password - the password of the account sending money
#
# sends money from one account to another
post '/transfers' do
  from, to = params.values_at('from', 'to').map(&:downcase).map(&:to_sym)
  amount = params['amount'].to_i
  password = params['password']

  # do not transfer money if there is not enough on the account
  return "Not enough money. Current balance #{BALANCES[from]}" if BALANCES[from] < amount
  return  'Wrong credentials' if PASSWORDS[from] != password

  BALANCES[from] -= amount
  BALANCES[to] += amount

  "Transfer successful. 
  
  User: #{from} | New Balance: #{BALANCES[from]}M$
  
  User: #{to} | New Balance: #{BALANCES[to]}M$
  "
end